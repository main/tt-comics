package org.fox.ttcomics2;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.OnViewTapListener;
import com.github.chrisbanes.photoview.PhotoView;

import org.fox.ttcomics2.archive.ComicArchive;

import java.io.IOException;

import androidx.appcompat.app.ActionBar;
import icepick.State;

public class ComicFragment extends StateSavedFragment {
	private final String TAG = this.getClass().getSimpleName();
	
	private SharedPreferences m_prefs;
	@State protected int m_page;
	private ViewComicActivity m_activity;
    @State protected ComicArchive m_archive;
    private PhotoView m_image;
	
	public ComicFragment() {
		super();
	}

    public void initialize(ComicArchive archive, int page) {
        m_archive = archive;
        m_page = page;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    	
		
		View view = inflater.inflate(R.layout.fragment_comic, container, false);

		m_image = view.findViewById(R.id.comic_image);

		try {
			Glide.with(getContext())
                .load(m_archive.getByteArray(m_page))
				.dontAnimate()
				.dontTransform()
				.diskCacheStrategy(DiskCacheStrategy.NONE)
				.skipMemoryCache(true)
                .into(m_image);
		} catch (IOException e) {
			m_image.setImageResource(R.drawable.badimage);
			e.printStackTrace();
		}

		if (m_prefs.getBoolean("fit_to_width", false)) {
			m_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
		}

		m_image.setOnViewTapListener(new OnViewTapListener() {
			@Override
			public void onViewTap(View view, float x, float y) {
				int width = view.getWidth();
				boolean fullScreenMode = m_prefs.getBoolean("use_full_screen", false);

				if (x <= width / 10) {
					m_activity.selectPreviousComic();
				} else if (x > width-(width/10)) {
					m_activity.selectNextComic();
				} else {
					ActionBar bar = m_activity.getSupportActionBar();

					if (bar.isShowing()) {
						bar.hide();
						m_activity.hideSeekBar(true);

						if (fullScreenMode) {
							m_activity.hideSystemUI(true);
						}

					} else {
						m_activity.hideSeekBar(false);

						if (fullScreenMode) {
							m_activity.hideSystemUI(false);
						}

						bar.show();
					}
				}
			}
		});

		return view;
		
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		m_prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
		m_activity = (ViewComicActivity) activity;
	}

}

package org.fox.ttcomics2;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;

import org.fox.ttcomics2.sync.SyncClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;
import icepick.State;

public class ViewComicActivity extends CommonActivity {
	private final String TAG = this.getClass().getSimpleName();

	@State protected String m_fileName;
	@State protected String m_tmpFileName;
	
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        m_prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        super.onCreate(savedInstanceState);

        if (m_prefs.getBoolean("prevent_screen_sleep", false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        setContentView(R.layout.activity_view_comic);
		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

		if (m_prefs.getBoolean("use_full_screen", false)) {
			hideSystemUI(true);
		}

		if (savedInstanceState == null) {
        	m_fileName = getIntent().getStringExtra("fileName"); 
        	
        	ComicPager cp = new ComicPager();
        	cp.setFileName(m_fileName);
        	
       		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
       		ft.replace(R.id.comics_pager_container, cp, FRAG_COMICS_PAGER);
       		ft.commit();
        }

        setOrientationLock(isOrientationLocked(), true);
        
       	getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		initSyncClient();

        setTitle(new File(m_fileName).getName());
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_view_comic, menu);
        
        menu.findItem(R.id.menu_sync_location).setVisible(m_prefs.getBoolean("use_position_sync", false) && m_syncClient.hasOwner());
        
        return true;
    }

	@Override
	public void onComicSelected(String fileName, int position) {
		super.onComicSelected(fileName, position);

		Log.d(TAG, "onComicSelected: " + fileName + " " + position);

		if ((position + 1) % 10 == 0 && m_prefs.getBoolean("use_position_sync", false) && m_syncClient.hasOwner()) {
			m_syncClient.setPosition(sha1(new File(fileName).getName()), m_databaseHelper.getLastPosition(fileName));
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		
		// upload progress
		if (m_prefs.getBoolean("use_position_sync", false) && m_syncClient.hasOwner()) {
    		//toast(R.string.sync_uploading);
    		m_syncClient.setPosition(sha1(new File(m_fileName).getName()), m_databaseHelper.getLastPosition(m_fileName));
    	}
	}
	
	private void shareComic() {
		
		ComicPager pager = (ComicPager) getSupportFragmentManager().findFragmentByTag(FRAG_COMICS_PAGER);
		
		if (pager != null) {
			
			try {
				File tmpFile = File.createTempFile("trcshare" + sha1(m_fileName + " " + pager.getPosition()), ".jpg", getCacheDir(this));
				
				Log.d(TAG, "FILE=" + tmpFile);
				
				InputStream is = pager.getArchive().getItem(pager.getPosition());
				
				FileOutputStream fos = new FileOutputStream(tmpFile);
				
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
				    fos.write(buffer, 0, len);
				}
				
				fos.close();
				is.close();

				m_tmpFileName = tmpFile.getAbsolutePath();

				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("image/jpeg");

				Uri uri = FileProvider.getUriForFile(this, "org.fox.ttcomics2.files", tmpFile);
				shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

				startActivityForResult(Intent.createChooser(shareIntent, getString(R.string.share_comic)), REQUEST_SHARE);

			} catch (IOException e) {
				toast(getString(R.string.error_could_not_prepare_file_for_sharing));
				e.printStackTrace();
			}

		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
	    if (requestCode == REQUEST_SHARE) {
	    	File tmpFile = new File(m_tmpFileName);
	    	
	    	if (tmpFile.exists()) {
	    		tmpFile.delete();
	    	}

	    }
	    super.onActivityResult(requestCode, resultCode, intent);
	}
	
	protected boolean isOrientationLocked() {
		return m_prefs.getBoolean("prefs_lock_orientation", false);
	}
	
	private void setOrientationLock(boolean locked, boolean restoreLast) {
		if (locked) {
			
			int currentOrientation = restoreLast ? m_prefs.getInt("last_orientation", getResources().getConfiguration().orientation) :					
					getResources().getConfiguration().orientation;
			
			if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
			   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			} else {
			   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		}

		if (locked != isOrientationLocked()) {
			SharedPreferences.Editor editor = m_prefs.edit();
			editor.putBoolean("prefs_lock_orientation", locked);
			editor.putInt("last_orientation", getResources().getConfiguration().orientation);
			editor.apply();
		}
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_share:
			shareComic();			
			return true;
		case R.id.menu_toggle_orientation_lock:
			setOrientationLock(!isOrientationLocked(), false);			
			return true;			
		case R.id.menu_sync_location:
	        m_syncClient.getPosition(sha1(new File(m_fileName).getName()), new SyncClient.PositionReceivedListener() {
				@Override
				public void onPositionReceived(final int position) {
					final ComicPager pager = (ComicPager) getSupportFragmentManager().findFragmentByTag(FRAG_COMICS_PAGER);
					
					if (pager != null && pager.isAdded()) {
						int localPosition = pager.getPosition();

						if (position > localPosition) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ViewComicActivity.this);
							builder.setMessage(getString(R.string.sync_server_has_further_page, localPosition + 1, position + 1))
									.setCancelable(true)
									.setPositiveButton(R.string.dialog_open_page, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											pager.setCurrentItem(position);
										}
									})
									.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});
							AlertDialog alert = builder.create();
							alert.show();
						} else if (position == -1) {
							toast("Could not contact sync server.");
						} else {
							toast(R.string.error_sync_no_data);
						}
						
					}				
				}
			});
	        return true;			
		case R.id.menu_go_location:			
			Dialog dialog = new Dialog(ViewComicActivity.this);
			AlertDialog.Builder builder = new AlertDialog.Builder(ViewComicActivity.this)
					.setTitle("Go to...")
					.setItems(
							new String[] {
									getString(R.string.dialog_location_beginning),
									getString(R.string.dialog_location_furthest),
									getString(R.string.dialog_location_location),
									getString(R.string.dialog_location_end) 
									},
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									
									final ComicPager cp = (ComicPager) getSupportFragmentManager().findFragmentByTag(CommonActivity.FRAG_COMICS_PAGER);

									switch (which) {
									case 0:
										cp.setCurrentItem(0);										
										break;
									case 1:
										cp.setCurrentItem(m_databaseHelper.getMaxPosition(m_fileName));
										break;
									case 2:										
										if (true) {
											LayoutInflater inflater = getLayoutInflater();
											View contentView = inflater.inflate(R.layout.dialog_location, null);
	
											final NumberPicker picker = contentView.findViewById(R.id.number_picker);
													
											picker.setMinValue(1);
											picker.setMaxValue(m_databaseHelper.getSize(m_fileName));
											picker.setValue(cp.getPosition() + 1);
	
											Dialog seekDialog = new Dialog(ViewComicActivity.this);
											AlertDialog.Builder seekBuilder = new AlertDialog.Builder(ViewComicActivity.this)
												.setTitle(R.string.dialog_open_location)
												.setView(contentView)
												.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
													
													public void onClick(DialogInterface dialog, int which) {
														dialog.cancel();													
													}
												}).setPositiveButton(R.string.dialog_open_location, new DialogInterface.OnClickListener() {
													
													public void onClick(DialogInterface dialog, int which) {
														dialog.cancel();
														
														cp.setCurrentItem(picker.getValue()-1);
														
													}
												});
											
											seekDialog = seekBuilder.create();
											seekDialog.show();
										}
										
										break;
									case 3:
										cp.setCurrentItem(cp.getCount() - 1);
										break;
									}
									
									dialog.cancel();
								}
							});

			dialog = builder.create();
			dialog.show();
			
			return true;
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			Log.d(TAG,
					"onOptionsItemSelected, unhandled id=" + item.getItemId());
			return super.onOptionsItemSelected(item);
		}
	}

    public void hideSeekBar(boolean hide) {
        ComicPager pager = (ComicPager) getSupportFragmentManager().findFragmentByTag(FRAG_COMICS_PAGER);

        if (pager != null) {
            pager.hideReadingUI(hide);
        }
    }

	@Override
	public void onBackPressed() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra("fileName", m_fileName);

		setResult(Activity.RESULT_OK, resultIntent);

		super.onBackPressed();
	}

}

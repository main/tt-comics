package org.fox.ttcomics2.junrar;


public interface UnrarCallback {

    boolean isNextVolumeReady(Volume nextVolume);

    void volumeProgressChanged(long current, long total);
}

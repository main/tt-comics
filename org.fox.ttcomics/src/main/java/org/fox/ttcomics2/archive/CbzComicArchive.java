package org.fox.ttcomics2.archive;

import android.os.Parcel;
import android.os.Parcelable;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import org.fox.ttcomics2.utils.NaturalOrderComparator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CbzComicArchive extends ComicArchive {
	private final String TAG = this.getClass().getSimpleName();

    private String m_fileName;
	private ZipFile m_zipFile;
	private ArrayList<String> m_entries = new ArrayList<>();
		
	@Override
	public int getCount() {
		return m_entries.size();
	}
	
	@Override
	public InputStream getItem(int index) throws IOException {
	    try {
            return m_zipFile.getInputStream(m_zipFile.getFileHeader(m_entries.get(index)));
        } catch (ZipException e) {
	        throw new IOException(e.getMessage());
        }
	}

    private void initialize() throws IOException {
	    try {
            m_zipFile = new ZipFile(m_fileName);

            List<FileHeader> fileHeaders = m_zipFile.getFileHeaders();

            for (int i = 0; i < fileHeaders.size(); i++) {
                FileHeader fh = fileHeaders.get(i);

                if (fh != null) {
                    if (!fh.isDirectory() && isValidComic(fh.getFileName())) {
                        m_entries.add(fh.getFileName());
                    }
                }
            }
        } catch (ZipException e) {
	        throw new IOException(e.getMessage());
        }

        Collections.sort(m_entries, new NaturalOrderComparator());
    }

	public CbzComicArchive(String fileName) throws IOException {
        m_fileName = fileName;

        initialize();
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(m_fileName);
    }

    public void readFromParcel(Parcel in) {
        m_fileName = in.readString();
    }

    public CbzComicArchive(Parcel in) throws IOException {
        readFromParcel(in);

        initialize();
    }

    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator CREATOR =
        new Parcelable.Creator() {
            public CbzComicArchive createFromParcel(Parcel in) {
                try {
                    return new CbzComicArchive(in);
                } catch (IOException e) {
                    e.printStackTrace();

                    return null;
                }
            }

            public CbzComicArchive[] newArray(int size) {
                return new CbzComicArchive[size];
            }
        };

}

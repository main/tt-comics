package org.fox.ttcomics2.archive;

import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class ComicArchive implements Parcelable {
	public abstract int getCount();
	public abstract InputStream getItem(int index) throws IOException;
	public boolean isValidComic(String fileName) {
		return fileName.toLowerCase().matches(".*\\.(jpe?g|bmp|gif|webp|png)$");
	}
    public byte[] getByteArray(int m_page) throws IOException {
        InputStream is = getItem(m_page);

        int size = 16384;
        byte[] buf = new byte[size];
        int len = 0;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        buf = new byte[size];
        while ((len = is.read(buf, 0, size)) != -1)
            bos.write(buf, 0, len);

        return bos.toByteArray();
    }
}

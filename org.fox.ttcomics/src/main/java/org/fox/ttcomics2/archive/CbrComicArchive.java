package org.fox.ttcomics2.archive;

import android.os.Parcel;
import android.os.Parcelable;

import org.fox.ttcomics2.junrar.Archive;
import org.fox.ttcomics2.junrar.exception.RarException;
import org.fox.ttcomics2.junrar.rarfile.FileHeader;
import org.fox.ttcomics2.utils.NaturalOrderComparator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class CbrComicArchive extends ComicArchive {
    private final String TAG = this.getClass().getSimpleName();

    private String m_fileName;
    private Archive m_archive;
    private ArrayList<CbrEntry> m_entries = new ArrayList<>();

    @Override
    public int getCount() {
        return m_entries.size();
    }

    @Override
    public InputStream getItem(int index) throws IOException {
        try {
            return new ByteArrayInputStream(getByteArray(index));
        } catch (NullPointerException e) {
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public byte[] getByteArray(int index) throws IOException {
        FileHeader fh = m_entries.get(index).m_fh;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            m_archive.extractFile(fh, out);

            return out.toByteArray();
        } catch (RarException e) {
            e.printStackTrace();

            return null;
        }
    }

    private void initialize() throws IOException {
        try {
            m_archive = new Archive(new File(m_fileName));
        } catch (RarException e) {
            e.printStackTrace();
            return;
        }

        while (true) {
            FileHeader fh = m_archive.nextFileHeader();

            if (fh == null)
                break;

            if (fh.isDirectory() || fh.isEncrypted())
                continue;

            m_entries.add(new CbrEntry(fh));
        }

        Collections.sort(m_entries, new NaturalOrderComparator());
    }

    public CbrComicArchive(String fileName) throws IOException {
        m_fileName = fileName;

        initialize();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(m_fileName);
    }

    public void readFromParcel(Parcel in) {
        m_fileName = in.readString();
    }

    public CbrComicArchive(Parcel in) throws IOException {
        readFromParcel(in);

        initialize();
    }

    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public CbrComicArchive createFromParcel(Parcel in) {
                    try {
                        return new CbrComicArchive(in);
                    } catch (IOException e) {
                        e.printStackTrace();

                        return null;
                    }
                }

                public CbrComicArchive[] newArray(int size) {
                    return new CbrComicArchive[size];
                }
            };

    private class CbrEntry {
        FileHeader m_fh;

        CbrEntry(FileHeader fh) {
            m_fh = fh;
        }

        @Override
        public String toString() {
            return m_fh.getFileNameString();
        }

    }
}

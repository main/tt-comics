package org.fox.ttcomics2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;

import androidx.appcompat.widget.Toolbar;

public class PreferencesActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // we use that before parent onCreate so let's init locally
        m_prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_preferences);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.replace(R.id.preferences_container, new PreferencesFragment());
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICK_DIRECTORY && resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {

            String path = data.getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR);

            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("comics_directory", path);
            editor.apply();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void startPicker(String initialDir) {
        Intent intent = new Intent(PreferencesActivity.this, DirectoryChooserActivity.class);

        if (initialDir == null) initialDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

        intent.putExtra(DirectoryChooserActivity.EXTRA_CONFIG, DirectoryChooserConfig.builder()
                .newDirectoryName("Comics")
                .allowNewDirectoryNameModification(true)
                .allowReadOnlyDirectory(true)
                .initialDirectory(initialDir)
                .build());

        startActivityForResult(intent, REQUEST_PICK_DIRECTORY);
    }
}

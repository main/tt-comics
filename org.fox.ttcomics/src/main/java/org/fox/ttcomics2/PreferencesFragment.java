package org.fox.ttcomics2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.fox.ttcomics2.sync.SyncClient;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PreferencesFragment extends PreferenceFragment {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

    }

    public boolean isGMSAvailable(Context context){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void onResume() {
        super.onResume();

        String version = "?";
        int versionCode = -1;
        String buildTimestamp = "N/A";

        try {
            Activity activity = getActivity();

            PackageInfo packageInfo = activity.getPackageManager().
                    getPackageInfo(activity.getPackageName(), 0);

            version = packageInfo.versionName;
            versionCode = packageInfo.versionCode;

            findPreference("version").setSummary(getString(R.string.prefs_version, version, versionCode));

            buildTimestamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date(BuildConfig.TIMESTAMP));
            findPreference("build_timestamp").setSummary(getString(R.string.prefs_build_timestamp, buildTimestamp));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext());

        findPreference("comics_directory").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ((PreferencesActivity)getActivity()).startPicker(null);
                return true;
            }
        });

        findPreference("comics_directory").setSummary(prefs.getString("comics_directory",
                getString(R.string.prefs_comics_directory_summary)));

        findPreference("open_storage_dir").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ((PreferencesActivity)getActivity()).startPicker("/storage");
                return true;
            }
        });

        findPreference("choose_sync_account").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (isGMSAvailable(getActivity())) {
                    Intent googlePicker = AccountPicker.newChooseAccountIntent(null, null,
                            new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, true, null, null, null, null);

                    getActivity().startActivityForResult(googlePicker, CommonActivity.REQUEST_SYNC_ACCOUNT);
                } else {
                    final EditText input = new EditText(getActivity());
                    input.setText(prefs.getString("sync_account", ""));
                    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getString(R.string.dialog_set_sync_account_manually))
                            .setView(input)
                            .setCancelable(true)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    String account = input.getText().toString().trim();

                                    if (isValidEmail(account)) {
                                        dialog.dismiss();

                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putString("sync_account", account);
                                        editor.apply();

                                        findPreference("choose_sync_account").setSummary(account);

                                    } else {
                                        ((PreferencesActivity)getActivity()).toast(R.string.dialog_invalid_sync_account_syntax);
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


                return true;
            }
        });

        findPreference("choose_sync_account").setSummary(prefs.getString("sync_account", getString(R.string.sync_account_not_set)));

        findPreference("clear_sync_data").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.dialog_clear_data_title)
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_clear_data, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String syncAccount = CommonActivity.getSyncAccount(getActivity().getApplicationContext());
                                SyncClient m_syncClient = new SyncClient();

                                if (syncAccount != null) {
                                    m_syncClient.setOwner(syncAccount);
                                } else {
                                    if (BuildConfig.DEBUG) {
                                        m_syncClient.setOwner("TEST-ACCOUNT");
                                    } else {
                                        ((CommonActivity)getActivity()).toast(R.string.error_sync_no_account);
                                    }
                                }

                                if (m_syncClient.hasOwner()) {
                                    m_syncClient.clearData(null);
                                }
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                return false;
            }
        });
    }

}
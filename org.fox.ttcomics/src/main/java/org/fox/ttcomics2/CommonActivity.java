package org.fox.ttcomics2;


import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.livefront.bridge.Bridge;

import org.fox.ttcomics2.sync.SyncClient;
import org.fox.ttcomics2.utils.CacheCleanupService;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

public class CommonActivity extends AppCompatActivity {
	private final String TAG = this.getClass().getSimpleName();

	protected static final String FRAG_COMICS_PAGER = "comic_pager";
	protected static final String FRAG_COMICS_LIST = "comics_list";

	protected final static int REQUEST_SHARE = 1;
	protected static final int REQUEST_VIEWCOMIC = 2;
	protected static final int REQUEST_PERMISSIONS_RESULT = 3;
	protected static final int REQUEST_SYNC_ACCOUNT = 4;
	protected static final int REQUEST_PICK_DIRECTORY = 5;

	public static final long MAX_CACHE_SIZE = 100 * 1024 * 1024; // bytes

	protected SharedPreferences m_prefs;
	protected SyncClient m_syncClient = new SyncClient();

	protected DatabaseHelper m_databaseHelper;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		Bridge.restoreInstanceState(this, savedInstanceState);

		m_databaseHelper = DatabaseHelper.getInstance(this);

        m_prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	public boolean isSyncEnabled() {
		return m_prefs.getBoolean("use_position_sync", false);
	}

	protected void initSyncClient() {
		Log.d(TAG, "initSyncClient");

    	if (isSyncEnabled()) {

			String syncAccount = getSyncAccount(this);

			if (syncAccount != null) {
				m_syncClient.setOwner(syncAccount);
			} else {
				if (BuildConfig.DEBUG) {
					m_syncClient.setOwner("TEST-ACCOUNT");
				} else {
					m_syncClient.setOwner(null);
					toast(R.string.error_sync_no_account);
				}
			}

    	} else {
    		m_syncClient.setOwner(null);
    	}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode,
									final Intent data) {

		if (requestCode == REQUEST_SYNC_ACCOUNT && resultCode == RESULT_OK) {
			String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

			SharedPreferences.Editor editor = m_prefs.edit();
			editor.putString("sync_account", accountName);
			editor.apply();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		Bridge.saveInstanceState(this, out);
	}

	public void onComicArchiveSelected(String fileName) {
		//
	}

	public void onComicSelected(String fileName, int position) {
    	m_databaseHelper.setLastPosition(fileName, position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(CommonActivity.this,
					PreferencesActivity.class);
			startActivityForResult(intent, 0);
			return true;
		default:
			Log.d(TAG,
					"onOptionsItemSelected, unhandled id=" + item.getItemId());
			return super.onOptionsItemSelected(item);
		}
	}

	protected static String md5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
	        digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
	        
	        StringBuffer hexString = new StringBuffer();
	        for (int i=0; i<messageDigest.length; i++)
	            hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
	        
	        return hexString.toString();
	        
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static String sha1(String s) {
		if (s != null) {
			try {
				MessageDigest digest = java.security.MessageDigest.getInstance("SHA1");
		        digest.update(s.getBytes());
                byte[] messageDigest = digest.digest();
		        
		        StringBuffer hexString = new StringBuffer();
		        for (int i=0; i<messageDigest.length; i++)
		            hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
		        
		        return hexString.toString();
		        
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public static File getThumbnailDir(Context ctx) {
		File baseDir = ctx.getFilesDir();

		File thumbDir = new File(baseDir, "thumbnails");

		if (!thumbDir.exists())
			thumbDir.mkdirs();

		return thumbDir;
	}

	public static File getCacheDir(Context ctx) {
		return ctx.getCacheDir();
	}

	public static String getThumbnailFileName(Context ctx, String fileName) {
		File file = new File(getThumbnailDir(ctx), md5(fileName) + ".png");

		return file.getAbsolutePath();
	}

	public static String getCacheFileName(Context ctx, String fileName) {
		File file = new File(getCacheDir(ctx), md5(fileName) + ".png");

		return file.getAbsolutePath();
	}
	
	public static String getSyncAccount(Context ctx) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		return prefs.getString("sync_account", null);
	}
	
	public void toast(int msgId) {
		toast(getString(msgId));
	}

	public void toast(String msg) {
		Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
				.setAction(R.string.dialog_close, new View.OnClickListener() {
					@Override
					public void onClick(View v) {

					}
				})
				.show();
	}

	public void cleanupCache() {
		Intent serviceIntent = new Intent();
		serviceIntent.putExtra("workDir", getCacheDir(getApplicationContext()).getAbsolutePath());

		JobIntentService.enqueueWork(getApplicationContext(), CacheCleanupService.class, 0, serviceIntent);

		serviceIntent = new Intent();
		serviceIntent.putExtra("workDir", getThumbnailDir(getApplicationContext()).getAbsolutePath());

		JobIntentService.enqueueWork(getApplicationContext(), CacheCleanupService.class, 0, serviceIntent);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void selectPreviousComic() {
		ComicPager frag = (ComicPager) getSupportFragmentManager().findFragmentByTag(FRAG_COMICS_PAGER);
		
		if (frag != null && frag.isAdded() && frag.getPosition() > 0) {
			frag.setCurrentItem(frag.getPosition() - 1);
		}		
	}

	public void selectNextComic() {
		ComicPager frag = (ComicPager) getSupportFragmentManager().findFragmentByTag(FRAG_COMICS_PAGER);
		
		if (frag != null && frag.isAdded() && frag.getPosition() < frag.getCount()-1) {
			frag.setCurrentItem(frag.getPosition() + 1);
		}		
	}


    public void hideSystemUI(boolean hide) {
        View decorView = getWindow().getDecorView();

		if (hide) {

			decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE);

		} else {

			decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

		}
	}
}

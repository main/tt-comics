package org.fox.ttcomics2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import org.fox.ttcomics2.archive.CbrComicArchive;
import org.fox.ttcomics2.archive.CbzComicArchive;
import org.fox.ttcomics2.archive.ComicArchive;

import java.io.IOException;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import icepick.State;

public class ComicPager extends StateSavedFragment {
	@State protected String m_fileName;
	private SharedPreferences m_prefs;
	private final String TAG = this.getClass().getSimpleName();
	private ComicArchive m_archive;
	private CommonActivity m_activity;
    private SeekBar m_seekBar;
    private TextView m_currentPage;
    private TextView m_totalPages;
	private View m_bottomBar;

    public void hideReadingUI(boolean hide) {
        m_bottomBar.setVisibility(hide ? View.GONE : View.VISIBLE);

		ActionBar actionBar = m_activity.getSupportActionBar();

		if (hide)
			actionBar.hide();
		else
			actionBar.show();

    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			ComicFragment cf = new ComicFragment();
            cf.initialize(m_archive, position);
			
			return cf;
		}

		@Override
		public int getCount() {
			return m_archive.getCount();
		}
	}

	private PagerAdapter m_adapter;
	
	public ComicPager() {
		super();
	}
	
	public ComicArchive getArchive() {
		return m_archive;
	}
	
	public int getCount() {
		return m_adapter.getCount();
	}
	
	public int getPosition() {
		ViewPager pager = getView().findViewById(R.id.comics_pager);
		
		if (pager != null) {
			return pager.getCurrentItem();
		}
		
		return 0;
	}

	public void setCurrentItem(int item) {
		ViewPager pager = getView().findViewById(R.id.comics_pager);
		
		if (pager != null) {
			try {
				pager.setCurrentItem(item);
                m_currentPage.setText(String.valueOf(item + 1));
                m_totalPages.setText(String.valueOf(m_archive.getCount()));
            } catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void setFileName(String fileName) {
		m_fileName = fileName;
	}
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    	
		
		final View view = inflater.inflate(R.layout.fragment_comics_pager, container, false);
	
		m_adapter = new PagerAdapter(getActivity().getSupportFragmentManager());
		
		final ViewPager pager = view.findViewById(R.id.comics_pager);
		
		try {
			if (m_fileName.toLowerCase().matches(".*\\.(cbz|zip)")) {									
				m_archive = new CbzComicArchive(m_fileName);
			} else if (m_fileName.toLowerCase().matches(".*\\.(cbr|rar)")) {
				m_archive = new CbrComicArchive(m_fileName);
			}

			final int position = m_activity.m_databaseHelper.getLastPosition(m_fileName);

            m_currentPage = view.findViewById(R.id.comics_page);
            m_totalPages = view.findViewById(R.id.comics_total_pages);

            m_currentPage.setText(String.valueOf(position + 1));
            m_totalPages.setText(String.valueOf(m_archive.getCount()));

			m_bottomBar = view.findViewById(R.id.comics_bottom_bar);

            m_seekBar = view.findViewById(R.id.comics_seek_bar);
            m_seekBar.setMax(m_archive.getCount() - 1);
            m_seekBar.setProgress(position);
            m_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					if (fromUser) {
						setCurrentItem(progress);
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});

			pager.setAdapter(m_adapter);
            pager.setCurrentItem(position);

            m_activity.onComicSelected(m_fileName, position);

            if (m_prefs.getBoolean("use_full_screen", false)) {
                m_activity.hideSystemUI(true);
            }

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					hideReadingUI(true);
				}
			}, 1000);

			
		} catch (IOException e) {
			m_activity.toast(R.string.error_could_not_open_comic_archive);
			e.printStackTrace();
		}

		pager.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {

				// don't react twice on every key press
				if (event.getAction() != KeyEvent.ACTION_UP)
					return true;

				switch (keyCode) {
					case KeyEvent.KEYCODE_SPACE:
					case KeyEvent.KEYCODE_DPAD_RIGHT:
						setCurrentItem(getPosition() + 1);
						return true;
					case KeyEvent.KEYCODE_DPAD_LEFT:
						setCurrentItem(getPosition() - 1);
						return true;
				}

				return false;
			}
		});

		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

				m_activity.onComicSelected(m_fileName, position);
                m_seekBar.setProgress(position);
				m_currentPage.setText(String.valueOf(position + 1));

                if (m_prefs.getBoolean("use_full_screen", false)) {
                    m_activity.hideSystemUI(true);
                }

				hideReadingUI(true);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		return view;		
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		m_activity = (CommonActivity) activity;
		
		m_prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
	}

}

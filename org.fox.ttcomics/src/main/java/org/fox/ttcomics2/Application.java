package org.fox.ttcomics2;

import android.os.Bundle;

import com.livefront.bridge.Bridge;
import com.livefront.bridge.SavedStateHandler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import icepick.Icepick;

public class Application extends android.app.Application {
    @Override
    public final void onCreate() {
        super.onCreate();

        Bridge.initialize(getApplicationContext(), new SavedStateHandler() {
            @Override
            public void saveInstanceState(@NonNull Object target, @NonNull Bundle state) {
                Icepick.saveInstanceState(target, state);
            }

            @Override
            public void restoreInstanceState(@NonNull Object target, @Nullable Bundle state) {
                Icepick.restoreInstanceState(target, state);
            }
        });
    }

}

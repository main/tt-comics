Pow! Comics Reader
==================

Minimalistic comics reader which syncs last read pages between devices.

Source: https://tt-rss.org/tt-comics

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Licensed under GNU GPL version 3.

Copyright (c) 2012 Andrew Dolgov (unless explicitly stated otherwise).
